# Datadog - Magento 

**This is still in beta status, it is stable but use at your own risk**

Datadog Magento integration handles sending events and metrics to Datadog from within the Magento application.

Metrics are collected and then sent via non-blocking UDP to the local Datadogstatsd.

All metrics are tagged with the following:

* magento.website
* magento.store_group
* magento.store
* magento.store_name
* magento.baseurl


## List of Metrics
Basic Counters:

* magento.core.page.view: Sends action and response code as tags.
* magento.core.page.view.time: Historgram of performance, using controller_front_init_before and controller_front_send_response_after as bookends.
* magento.customer.login: Simple counter, does not store result as a tag.
* magento.admin.login: This sends admin username and result of login as tags.
* magento.catalog.product.view: Stores the sku as a tag.
* magento.catalog.product.sendfriend: Stores the sku as a tag.


Admin Counters to track admin usage

* magento.index.refresh: Indexing events, lists the index type, user and other info
* magento.cache.refresh.all: Any cache clearing event, lists the cache type and username as tags.
* magento.cache.flush.storage: "Flush Cache Storage" button clicked or via script, lists the username as tag.
* magento.cache.flush.system: "Flush Magento Cache" button clicked or via script, lists the username as tag.
* magento.cache.flush.media: "Flush Catalog Image Cache" button clicked or via script, lists the username as tag.
* magento.cache.flush.js_css: "Flush Javascript/CSS Cache" button cliecked or via script, lists the username as tag.


Collected Via Cron: Meter like data, useful metrics most collected on a per day basis. 
This data is collected via Magento cronjob every 5 minutes.

* magento.index.status: Each one is tagged by name. Shows 0 if requires reindex, 1 if it is good.
* magento.cache.status: Each one is tagged by name. Shows 0 if requires clearing, 1 if it is good.
* magento.cron.queue.run: Meter shows cron runs.
* magento.cron.queue.total: Meter shows how many cron jobs are in Magentos queue
* magento.cron.queue.pending: Pending jobs count
* magento.cron.queue.died: Jobs that failed during execution.
* magento.cron.queue.error: Jobs that had an error, but still completed.
* magento.cron.queue.success: Jobs that completed correctly.
* magento.visitors.online: Shows a meter for the currently logged in users.
* magento.sales.today.count: Total number of orders today.
* magento.sales.today.grand_total: Grand total sales today
* magento.sales.today.shipping_total: Total of all shipping today.


## List of Events
Events are currently not supported because of the requirement of using TCP requests to the datadog API.

## Compatibility
CE 1.9.x   (TBD)

## Todo

* Several missing metrics planned
* Refactor code, its still in roughed in state
* Add events processing (blocking TCP Issue)
* Add redis queueing, this will allow for events processing
* Add more sys config settings, including host for datadog
* Ensure that when extension is toggled off, its not collecting stats

## License

TBD

## History

* Datadog Statsd integration based on the Datadog project - Magento Hackathon Zürich 2014
https://github.com/magento-hackathon/Hackathon_Datadog