<?php
/**
 * This file was part of Datadog_Metrics for Magento.
 * (Modified and used as part of the updated datadog integration)
 *
 * @license OSL v3
 * @author Jacques Bodin-Hullin <j.bodinhullin@monsieurbiz.com> <@jacquesbh>
 * @category Hackathon
 * @package Datadog_Metrics
 * @copyright Copyright (c) 2014 Magento Hackathon (http://mage-hackathon.de)
 */

class Datadog_Metrics_Model_Queue
{
    protected $_messages = array();

    /**
     * add a key/value pair to the stack
     *
     * @param $key
     * @param $value
     * @return Datadog_Metrics_Model_Queue
     */
    public function addMessage($key, $value = null, $tags = [], $type = Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_INCREMENT)
    {
        $this->_messages[] = [
            'key'   => $key,
            'type'  => $type,
            'value' => $value,
            'tags'  => $tags,
        ];
        return $this;
    }

    public function __destruct()
    {
        $this->_sendMessages();
    }

    public function sendMessages(){
        $this->_sendMessages();
    }
    
    protected function _sendMessages()
    {
        $store  = Mage::app()->getStore();
        $baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $mainTags = [
            'magento.website'     => $store->getWebsite()->getCode(),
            'magento.store_group' => $store->getGroupId(),
            'magento.store'       => $store->getCode(),
            'magento.store_name'  => $store->getName(),
            'magento.baseurl'     => $baseUrl,
        ];
        foreach ($this->getActiveChannels() as $channel) {
            if (!$channel instanceof Datadog_Metrics_Model_Channel_Interface) {
                throw new ErrorException("Your channel doesn't implement the channel interface.");
            }
            foreach ($this->_messages as $data) {
                $channel->send($data['key'], $data['value'], $data['tags'] + $mainTags, $data['type']);
            }
            $this->_messages = [];
        }
    }

    /**
     * get list of all active channels to push data to
     *
     * @return array
     */
    protected function getActiveChannels()
    {
        return Mage::getModel('datadog_metrics/config')->getActiveChannels();
    }
}
