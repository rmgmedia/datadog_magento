<?php
class Datadog_Metrics_Model_Observer_Catalog
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function catalogControllerProductView(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getProduct();

        $key = 'magento.catalog.product.view';
        $tags = array();
        $tags['magento.catalog.product.sku'] = $product->getSku();
        
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        $queue->addMessage($key,null,$tags);
    }

    public function sendfriendProduct(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getProduct();

        $key = 'magento.catalog.product.sendfriend';
        $tags = array();
        $tags['magento.catalog.product.sku'] = $product->getSku();
        
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
        $queue->addMessage($key,null,$tags);
    }
}