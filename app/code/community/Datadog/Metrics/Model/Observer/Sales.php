<?php

class Datadog_Metrics_Model_Observer_Sales
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderPlaceAfter(Varien_Event_Observer $observer)
    {
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        $queue->addMessage('magento.sales.order.count');
            
        // Histogram has a 10 second bucket and does not do SUM, which makes the usage
        // Of the totals not work here.
        // "So we really need a sum aggregator for the histogram then. Will see what I can find on that"
        
        // Order currency
        //$queue->addMessage('magento.sales.order.total.histogram', $order->getGrandTotal(), [
        //    'currency' => $order->getOrderCurrency()
        //], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_HISTOGRAM);

        // Base currency
        //$queue->addMessage('magento.sales.order.base.histogram', $order->getBaseGrandTotal(), [
        //    'currency' => $order->getBaseCurrency()
        //], Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_HISTOGRAM);

        return;
    }
}