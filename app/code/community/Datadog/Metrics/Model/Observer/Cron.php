<?php

class Datadog_Metrics_Model_Observer_Cron
{
    public function cronRun(Varien_Event_Observer $observer)
    {
        $key = $key = 'magento.cron.run';
        $tags = array();
        $tags['magento.executable'] = $_SERVER['SCRIPT_NAME'];
        $tags['magento.shell.user'] = $_SERVER['USER'];
    
        /** @var Datadog_Metrics_Model_Queue $queue */
        $queue = Mage::getSingleton('datadog_metrics/queue');
    
        $queue->addMessage($key,[],$tags);

    }    
    
}