<?php

class Datadog_Metrics_Model_Observer_Core
{
    /*
     * Collects the start time on controller_front_init_before
     */
    public function frontInitBefore(Varien_Event_Observer $observer){
        Mage::register('datadog_start', microtime(true));
    }
    
    /*
     * Outputs the mage.core.page.view and time metrics
     */    
    public function finalEvent(Varien_Event_Observer $observer){
        $queue = Mage::getSingleton('datadog_metrics/queue');
        $key = 'magento.core.page.loadtime';
        $datadogHistogram = Datadog_Metrics_Model_Config::CHANNEL_MESSAGE_TYPE_HISTOGRAM;
        
        $startTime = Mage::registry('datadog_start');
        $endTime = microtime(true);
        $executionTime = $endTime - $startTime;
        
        $response_code = $observer->getEvent()->getFront()->getResponse()->getHttpResponseCode();
 
        $request = Mage::app()->getRequest();
        $module = $request->getControllerModule();
        $module_controller = $request->getControllerName();
        $module_controller_action = $request->getActionName();
        
        $fullActionName = strtolower ($module."_".$module_controller."_".$module_controller_action);

        $tags = array();
        $tags['magento.action'] = $fullActionName;
        $tags['magento.path'] = $request->getRequestString();
        $tags['magento.response_code'] = $response_code;
        
        $queue->addMessage('magento.core.page.view',[],$tags);
        $queue->addMessage('magento.core.page.view.time',$executionTime,$tags,$datadogHistogram);

    }    
        
}