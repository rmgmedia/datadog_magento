<?php
/**
 * This file was part of Datadog_Metrics for Magento.
 * (Modified and used as part of the updated datadog integration)
 *
 * @license OSL v3
 * @author Jacques Bodin-Hullin <j.bodinhullin@monsieurbiz.com> <@jacquesbh>
 * @category Hackathon
 * @package Datadog_Metrics
 * @copyright Copyright (c) 2014 Magento Hackathon (http://mage-hackathon.de)
 */

/**
 * Channel_Interface Interface
 * @package Datadog_Metrics
 */
interface Datadog_Metrics_Model_Channel_Interface
{

    /**
     * Send data
     * @param string $key The metric key
     * @param string|int $value The metric value
     * @param array $tags The metric tags
     * @param string $type The metric type
     */
    public function send($key, $value, array $tags, $type);

}
